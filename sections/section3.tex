\newpage
\section{Major Machine Learning Algorithms}\label{majorAlgorithms}
In the following subsections, the three most commonly used machine learning algorithms are discussed. Most attention is given to support vector machines as they are the most complicated of the three and also the focus of this document.

\subsection{Linear Regression}
In linear regression learning algorithm, we assume there is a linear relationship between the inputs and their corresponding outputs. Continuing the house price example, we will try to find a linear relationship between the output ($y$); the price of the house, and the input ($x$); the area of the house.\\

A single variable linear relation between these variables can be represented as:

\begin{equation}
	y = \theta_0 + \theta_1 * x	
\end{equation}

or,

\begin{equation}
	y = \boldmath{\theta^T} \times \boldmath{X}	
\end{equation}

where,
\begin{center}
	$\boldmath{\theta^T}$ = $\begin{pmatrix}
					 \theta_0 & \theta_1
				\end{pmatrix}$ = the parameters that define the relation between inputs and outputs
\end{center}

\begin{center}
	$\boldmath{X}$ = $\begin{pmatrix}
					 1\\
					 x
				\end{pmatrix}$ 
\end{center}

From the above notation, we can see that `h' from figure \ref{learn} is actually:

\begin{equation}
    \label{hypothesisEquation}
    h_\theta(x) = \theta_0 + \theta_1 \times x	
\end{equation}

The task of the learning algorithm is to find best [$\theta_0$, $\theta_1$] for which the \textit{cost function} is minimum. The cost function for linear regression is\footnote{Machine Learning Lecture Notes by Andrew Ng}:

\begin{equation}
	J(\theta_0,\theta_1) = \frac{1}{2m}\sum_{i=1}^{m}({h_\theta(x^{(i)}) - y^{(i)}})^2
\end{equation}
where,

\begin{center}
	$i$ = $i^{th}$ training sample
\end{center}
	
\begin{center}
	$m$ = Total number of training samples
\end{center}

\begin{center}
	$y^{(i)}$ = Output of $i^{th}$ training sample
\end{center}

\begin{center}
	$x^{(i)}$ = Input of $i^{th}$ training sample
\end{center}

\begin{center}
	$h_\theta$ = The hypothesis function; which will express the relationship between inputs and outputs once it has been \textit{trained}
\end{center}

\begin{center}
	$J(\theta_0,\theta_1)$ = The cost function
\end{center}

This cost function is derived using various mathematical derivations and assumptions, whose discussion is beyond the scope of this report. Once training is complete, we will have values of [$\theta_0$, $\theta_1$] which will offer the minimum possible cost function.\\

For example, after training with data of figure \ref{house}, we will find definite values for [$\theta_0$, $\theta_1$]. Equation \ref{hypothesisEquation} will denote a line now. The prices for houses would now be estimated using this line, as shown in Figure \ref{house2}. Multivariate Linear regression is used if there are more than one input feature.

\begin{figure}
    \centering
    \includegraphics[width=3.5in]{media/house2.jpg}
    \caption{The line represents the relation found between inputs and outputs for the given training samples using linear regression.}
    \label{house2}
\end{figure}


\subsection{Logistic Regression}
Logistic regression uses a logistic, or sigmoid, function (shown in figure \ref{sigmoid}) to map the inputs to the outputs. The hypothesis function is:

\begin{equation}
	\label{logisticHypothesis}
	h_\theta(x) = \frac{1}{1-e^{-\theta^T x}}
\end{equation}

\begin{figure}[h]
    \centering
    \includegraphics[width=3in]{media/sigmoid.jpg}
    \caption{The hypothesis function for logistic regression is a sigmoid function.}
    \label{sigmoid}
\end{figure}

Since the output of a sigmoid function lies in the range [0,1], logistic regression is much suited for classification problems. In classification problems, we need to find the probability that a certain input belongs to category A or not. Since the probability also lies in the range [0,1], we can model that probability using logistic regression. We can predict, with some errors, that:

\begin{center}
	$y=1$ if $h_\theta(x) \geq 0$
\end{center}

And,

\begin{center}
	$y=0$ if $h_\theta(x) < 0$
\end{center}

As seen in figure \ref{logisticClassification}, single variable logistic function is used to define a decision boundary between two classes of objects.\\

\begin{figure}
    \centering
    \includegraphics[width=3in]{media/logisticClassification.png}
    \caption{Logistic regression is used to design a boundary between two classes.}
    \label{logisticClassification}
\end{figure}


Equation \ref{logisticHypothesis} represents a single variable logistic function. A more general logistic function would be:

\begin{equation}
	\label{logisticHypothesis2}
	h_\theta(\boldmath{X}) = \frac{1}{1-e^{- \boldmath{\theta}^T \boldmath{X}}}
\end{equation}

where,
\begin{center}
	$\boldmath{X}$ = $\begin{pmatrix}
					 1\\
					 x_0\\
					 x_1\\
					 \vdots\\
					 x_n
				\end{pmatrix}$ = Inputs vector with $n$ features
\end{center}

\begin{center}
	$\boldmath{\theta^T}$ = $\begin{pmatrix}
					 \theta_0 &\theta_0 & \theta_0 & \cdots & \theta_n \\
				\end{pmatrix}$ = Parameters vector with $n+1$ parameters
\end{center}

The cost function for logistic regression is defined as\footnote{Machine Learning Lecture Notes by Andrew Ng}:

\begin{equation}
	\label{logisticCost}
	%h_\theta(\boldmath{X}) = \frac{1}{1-e^{- \boldmath{\theta}^T \boldmath{X}}}
	J(\theta) = - \frac{1}{m} [\sum_{i=1}^{m} y^{(i)} log  h_\theta(x^{(i)}) + (1-y^{(i)}) log  (1-h_\theta (x^{(i)})) ]
\end{equation}

The first term in the summation is zero for training sample belonging to class $y=0$. Similarly, the second term in the summation is zero for training samples belonging to class $y=1$. It should be noted that the above cost function is actually the combination of:

\begin{equation}
	\label{logisticCost2}
	Cost(h_\theta(x), y) = \left\{	\begin{array}{r l}
										- log(h_\theta(x))& if y = 1 \\
										- log(1- h_\theta(x))& if y = 0 \\	
									\end{array}
							\right.
\end{equation}



%Logistic regression uses a sigmoid function to express the relationship between the outputs and the inputs. Although logistic regression is more expensive computationally as compared to linear regression, but it also finds a more realistic relationship since most of the real-world trends are not linear.
%Linear regression offers a computationally efficient way of relating the outputs to inputs. 

 

\subsection{Support Vector Machine}
A Support Vector Machine (SVM) is a classifier that uses hyperplanes to separate the classes. In other words, given labeled training data (supervised learning), the algorithm outputs an optimal hyperplane which categorizes new examples. SVM is much more geometrically motivated. Instead of trying to fit a particular model to the data (linear or logistic), we try to find the hyperplane that best separates the classes; the optimal hyperplane. SVM doesn't use any probabilistic model like linear or logistic regression. While the task of the logistic regression is to optimize the parameters of the logistic function so that the cost function is minimized, the SVM tries to find the best boundary layer that separates the data classes with the maximum possible margin. Consequently, SVM is also called the Large Margin Classifier.\\

The main difference between SVM and other classifiers is depicted in figure \ref{svm1} and figure \ref{svm2}.

\begin{figure}[h]
    \centering
    \includegraphics[width=3in]{media/svm1.png}
    \caption{Various separating straight lines can be used as classifiers for 2D data}
    \label{svm1}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=3in]{media/svm2.png}
    \caption{SMV finds the hyperplane (straight line in this case) that maximizes the margin}
    \label{svm2}
\end{figure}

\subsubsection{Computing the Optimal Hyperplane}

\textbf{Note:} In-depth explanation is beyond the scope of this document. Inquiring minds can see the  section 4.5 (Seperating Hyperplanes) of the book: Elements of Statistical Learning by T. Hastie, R. Tibshirani and J. H. Friedman.\\

A hyperplane can be defined as:

\begin{equation}
	\label{svmEq1}
	f(x) = \theta_0 + \theta^T x
\end{equation}


where,
\begin{center}
	$\theta_0$ = Bias
\end{center}

\begin{center}
	$\theta^T$ = Weight vector
\end{center}

\begin{center}
	$x$ = Input features
\end{center}


Conventionally, the optimal hyperplane is represented as:
%The optimal hyperplane can be represented in an infinite number of different ways by scaling of $\theta$ and $\theta_0$. As a matter of convention, among all the possible representations of the hyperplane, the one chosen is:

\begin{equation}
	| \theta_{0} + \theta^{T} x | = 1
	\label{optimalhyperplane}
\end{equation}

where $x$ represent the training examples that lie closest to the hyperplane; also known as the support vectors. This notation of the optimal hyperplane is called canonical hyperplane.\\
%where x symbolizes the training examples closest to the hyperplane. In general, the training examples that are closest to the hyperplane are called support vectors. This representation is known as the canonical hyperplane.

Using geometry analysis, we can see that the distance between a point $x$ and a hyperplane ($\theta$, $\theta_{0}$) is:

\begin{equation}
	\label{distance}
	distance = \frac{|\theta_{0} + \theta^{T} x|}{||\theta||}
\end{equation}

From (\ref{optimalhyperplane}) and (\ref{distance}), we can see that:

\begin{equation}
distance_{support vectors} = \frac{|\theta_{0} + \theta^{T} x|}{||\theta||} = \frac{1}{||\theta||}
\end{equation}\\


Since the margin $M$ is twice the distance to the closest examples:

\begin{equation}
	M = \frac{2}{||\theta||}
\end{equation}

Finally, the problem of maximizing $M$ is equivalent to the problem of minimizing a function $J(\theta)$ subject to some constraints. The constraints model the requirement for the hyperplane to classify correctly all the training examples $x_{i}$. Formally,

\begin{equation}
	\label{svmCost}
	\min_{\theta, \theta_{0}} J(\theta) = \frac{1}{2}||\theta||^{2} \text{ subject to } y_{i}(\theta^{T} x_{i} + \theta_{0}) \geq 1 \text{ } \forall i
\end{equation}

where $y_{i}$ represents each of the labels of the training examples.\\

This is a problem of Lagrangian optimization that can be solved using Lagrange multipliers to obtain the weight vector $\theta$ and the bias $\theta_{0}$ of the optimal hyperplane.\\

\textit{Furthermore, a hyperplane can also be denoted using a dot-product notation. Since efficient digital algorithms exist to calculate the dot product of two vectors, it implies that finding a hyperplane is very compute-efficient process.\\}

\subsubsection{Kernel Trick (Non-Linear Classification)}

It can be seen from Eq. \ref{svmEq1} that SVM generates a linear separation boundary. It means that SVM can not establish a non-linear pattern in the input data. However, we can use a technique called the \textit{Kernel Trick} to train and analyze non-linear datasets:

\begin{displayquote}
A dataset $D$ that is not linearly separable in $\mathbb{R}^N $ may be linearly separable in a higher-dimensional space $\mathbb{R}^M $ (where $M > N$). Thus, if we have a transformation $k$ that lifts the dataset $D$ to a higher-dimensional $D'$ such that $D'$ is linearly separable, then we can train a linear SVM on $D'$ to find a decision boundary $\vv{\omega}$ that separates the classes in $\mathbb{R}^M $ . Projecting the decision boundary $\vv{\omega} $ found in $\mathbb{R}^M $ back to the original space $\mathbb{R}^N $ will yield a nonlinear decision boundary.\footnote{www.eric-kim.net}
\end{displayquote}

\subsubsection*{Intuition}
Kernel function is actually a transformation function whose job is to raise the dimensions of the feature space. In this way, a linear boundary can be established in the raised dimensions which was impossible in the actual feature space (Figure \ref{kernel1} and figure \ref{kernel2}).

It is called a \textit{trick} because SVM has no need to explicitly work in the higher-dimensional space at training or testing time. During training, the optimization problem only uses the training examples to compute pair-wise dot products $\langle \vv{\bm{x_i}}, \vv{\bm{x_j}} \rangle$ , where $x_i, x_j \in \mathbb{R}^N $ .

\subsubsection*{Efficiency}
It turns out that there exist functions that can implicitly compute the dot-product between two vectors in a raised-dimensions vector space \textbf{without explicitly transforming those vectors from $\mathbb{R}^N$ to $\mathbb{R}^M $}.  Such functions are called kernel functions. Kernel functions compute dot-product in a higher-dimensional vector space $\mathbb{R}^M$ while remaining in $\mathbb{R}^N$

\begin{figure}[h]
    \centering
    \includegraphics[width=5in]{media/data_2d_to_3d.png}
    \caption{A non-linearly separable dataset in $\mathbb{R}^2$ (left) is transformed into a linearly separable vector space in $\mathbb{R}^3$ (right) using a quadratic kernel function}
    \label{kernel1}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=5in]{media/data_2d_to_3d_hyperplane.png}
    \caption{The decision boundary appears linear to SVM in $\mathbb{R}^3$ (left) because of the kernel function. The decision boundary is non-linear in $\mathbb{R}^2$ (right)}
    \label{kernel2}
\end{figure}