\section{Machine Learning} \label{machineLearning}
There is no universally accepted definition of machine learning. In 1959, American pioneer in intelligent computer gaming and artificial intelligence, Arthur Samuel, described machine learning as:

\begin{displayquote}
\textit{The field of study that gives computers the ability to learn without being explicitly programmed}
\end{displayquote}

Machine learning became a famous topic of interest specially among computer science researchers in the last decade of the 20th century. Because of the endless opportunities it offered to understand the meaning of heaps of data, people from other disciplines also studied machine learning and applied it in fields as diverse as molecular research and business intelligence. In 1998, leading game theory researcher Tom Mitchell put forward a more scientific definition of machine learning:

\begin{displayquote}
\textit{A computer program is said to learn from experience E with respect to some task T and some performance measure P, if its performance on T, as measured by P, improves with experience E. }
\end{displayquote}

\subsubsection*{Intuition}
Lets take an example to understand the above definition. Suppose we have an email server program whose job is also to find and block spam emails. Different users mark various emails as spam mails. The computer software (i.e, the email server) tries to identify some core features that will help the program to identify other emails as spam. As more and more users mark more and more emails as spam, the program gets a better and better understanding of spam mails. This lets the email server to mark a new email, previously unknown to the program, as spam based on the data provided by users.\\

In reference to above definition, task T is finding if a certain email is spam or not, performance measure P is the percentage of spam mails that the program identified and blocked successfully, experience E is the pattern (or knowledge) that program identified by analyzing the spam mails.\\

In simple words, we provide some data sets to a computer program. The computer program goes through all the data and tries to find some pattern or relation between the inputs and the outputs and then makes an educated estimate about the output of an unknown input based on the pattern that the program discovered.

\subsection{Basic Concepts}
The email example shows us that a program can \textit{learn} how to identify a spam mail without being explicitly programmed to do so. Through another example, the following four subsections discuss the overall approach used to design a self-learning program.

\subsubsection{What are Features?}
In the domain of AI, `features' mean attributes. Each attribute (or property) of an object is its feature. Consequently an object may have infinite many features, in the same way as it may have infinite many properties (or attributes).

For example, a car has typically four tyres, two side mirrors, one driving wheel, two head lights etc. All these things are `features' of a car. Table 1 shows some possible features of cars.
\begin{table}[h]
	 \caption{Some possible features of cars}
	\begin{center}
		\begin{tabular}{l c}
		  Feature&Value\\
		  \hline	
		  Number of tyres & 4\\
		  Number of side mirrors & 2\\
		  Number of driving wheels & 1\\
		  Number of head lights & 2\\
		  Seating capacity & 2 or 4\\
		\end{tabular}
	
	\end{center}
\end{table}

It can be seen from the table 1 that the number of features can be infinite. However in most cases, only a few features are of interest. 

\subsubsection{Feature Selection}
While an object may have many features, only those features are desired that are unique in that object (or class\footnote{A class refers to a group of objects that have similar features.}). Such features would let the classifier discriminate that object from others. Selection of the right features is a critical step in designing an intelligent system. Poor selection of features will poorly 'train' the system, hence, the system would fail to identify the signal among a bunch of signals.

Continuing the above example, the selected feature of a car might be the seating capacity of that car. This feature can help the software identify a sports car from a family car (assuming all sports cars  are twin-seated). Table 2 shows various features of a car along with their effectiveness for classification.

\begin{table}[h]
		\caption{Useful features for a car's classification}
	\begin{center}
		\begin{tabular}{l c c}
		  Feature&Value&Useful for class identification\\
		  \hline	
		  Number of tyres & 4& X\\
		  Number of side mirrors & 2 & X\\
  		  Number of driving wheels & 1& X\\
 		  Number of head lights & 2&X\\
  		  Seating capacity & 2 or 4&\checkmark\\
		\end{tabular} 
	\end{center}
\end{table}

For complex inputs, such as electrical signals, there are different types of criteria that automatically suggest what features offer the most separability.

\subsubsection{Feature Extraction}
Once such features have been identified that are uniquely related to an object (or class of objects), the next task is to develop an algorithm that can extract those features from the provided data. In the above example, the seating capacity of a car may be found by using image processing techniques.

\subsubsection{Finding a Pattern or Relation}
Usually a mathematical function is used to go through all the training data. This process trains the function by tuning different parameters of the function which can be used to estimate the likely output of an unknown input, as shown in Figure \ref{learn}. 

Some common learning algorithms are \textbf{linear regression learning algorithm}, \textbf{logistic regression learning algorithm}, \textbf{support vector regression learning algorithm}, etc. These learning algorithms employ a \textit{cost function} and during training, such parameters are found that produce the lowest cost function.


\begin{figure}
    \centering
    \includegraphics[width=3in]{media/learn.jpg}
    \caption{`h' is called the hypothesis function, that has been trained according to the training data. This function is now ready to produce estimated outputs for unknown inputs}
    \label{learn}
\end{figure}

In the car example, a algorithm would find out quickly that the defining feature is the number of seats. Consequently, it would simply see if the value of the extracted feature (the seating capacity of the car) is more than two or not. If it turns out to be two, the function would guess that the car is a sports car. If it turns out to be greater than two, the function would guess the car is a family car. Please refer to section \ref{majorAlgorithms} for details of the commonly used learning algorithms.



\subsection{Supervised Learning}
In supervised learning problems, we first provide data to the computer program. The program tries to find some relation (in technical terms, the program trains itself according to the training data) and then we can provide the unknown inputs so that the program may estimate an output.

The email server problem is an example of supervised learning in which we provide some marked mails to the program on which the program trains itself. There are two major categories of supervised learning problems:

\begin{enumerate}
\item{Classification problems}
\item{Regression problems}
\end{enumerate}


\subsubsection{Classification Problems}
In classification problems, we need to find if the output of an unknown input will or will not belong to a certain class. For example, in the car example we only needed to know if an unknown car is a sports car or a family car. Similarly, in the email server example, we only needed to know if a particular unknown email was spam or not.

In such problems, the training data is split between multiple categories\footnote{The geometric object (curve, surface, hypersurface) that splits the vector space is called the decision boundary}. The learning algorithm is explicitly told if a certain sample belongs to class A or class B. For example, an email is either marked as a spam mail or a normal mail. Consequently, the job of the classifying function is to find which class does an unknown input belong to.

In other words, \textbf{there is a limited set of possible outputs (classes)}. Each input is expected to fall in one of the possible output category.

\begin{table}[h]
		\caption{Possible outputs of some trivial classification problems}
	\begin{center}
		\begin{tabular}{l l l}
		  Problem&Possible output categories (classes)&Type of output\\
		  \hline	
		  Email server example & Email is either spam or not& Discrete valued\\
		  Car example & Car is either a sports car or a family car& Discrete valued\\
  		  Type of cancer& Either malignant or benign& Discrete valued\\
		\end{tabular} 
	\end{center}
\end{table}

\subsubsection{Regression Problems}
Lets assume a particular business estimates the price of a house. For the sake of simplicity, the price of the house is assumed to a function of only one variable: the area of the house. The business has some data about the prices of houses that were sold in the past. This data is shown in figure \ref{house}.

\begin{figure}[h]
    \centering
    \includegraphics[width=3in]{media/house.jpg}
    \caption{The data available to the business shows the prices of sold houses against their area. This data will be the training set for the learning algorithm}
    \label{house}
\end{figure}

Using this available data, the learning algorithm will design a function that would estimate the price of a house based on its area alone. This type of problems where \textbf{the output will be a real number, with infinite many possible values}, are called regression problems.

\begin{table}[h]
		\caption{Possible outputs of some trivial regression problems}
	\begin{center}
		\begin{tabular}{l l l}
		  Problem&Possible outputs&Type of output\\
		  \hline	
		  House price example & Price of house& Real valued\\
		  Temperature forecasting & Temperature at a particular location or time & Real valued\\
  		  Sales estimation & Number of sales & Real valued\\
		\end{tabular} 
	\end{center}
\end{table}


\subsection{Unsupervised Learning}
In all the examples discussed so far, all the training samples had a output label. It means that each email is either tagged as spam or normal mail; each car was tagged either as a sports car or a family car; each house had a associated price against its base area. All these cases are examples of \textit{supervised} learning as we explicitly told the learning algorithm the output to each training input. The job of the learning algorithm is just to find some relation between the inputs and the outputs.

The other type of learning algorithms are called unsupervised learning algorithms as their task is to find patterns among the inputs themselves. In this case, the learning algorithm is only provided the input data, without its corresponding outputs. The learning algorithm tries to find some common traits among the inputs. The most common application of unsupervised learning is \textit{clustering}:



\begin{displayquote}
\textit{Cluster analysis or clustering is the task of grouping a set of objects in such a way that objects in the same group (called a cluster) are more similar (in some sense or another) to each other than to those in other groups (clusters). It is a main task of exploratory data mining, and a common technique for statistical data analysis, used in many fields, including machine learning, pattern recognition, image analysis, information retrieval, and bioinformatics. Cluster analysis itself is not one specific algorithm, but the general task to be solved. It can be achieved by various algorithms that differ significantly in their notion of what constitutes a cluster and how to efficiently find them.}[Wikipedia]
\end{displayquote}

An example of clustering of large data set is shown in figure \ref{clustering}.


\newpage
\begin{figure}[h]
    \centering
    \includegraphics[width=3in]{media/clustering.jpg}
    \caption{Large numbers of irrelevant training data is provided to the learning algorithm. It divides the data into multiple clusters without the need of providing class labels}
    \label{clustering}
\end{figure}

