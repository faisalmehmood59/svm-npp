# Support Vector Machines (SVM) and Nuclear Power Plants: An Application of Artificial Intelligence in Nuclear Engineering #

This repository contains a latex document regarding SVMs and NPPs. This was my mid-term report in the course on Nuclear Instrumentation and Control.

## How To Download ##

###Method 1: Download the repository as a ZIP archive###
To download the repository as a ZIP archive, [Click here](https://bitbucket.org/faisalmehmood59/svm-npp/get/ba8b4054f75a.zip). Unzip the archive when download completes.


###Method 2: Download the git repository###
You would know how.

##Contact me##
You can reach me at: faisalmehmood59 <at> hotmail <dot> com