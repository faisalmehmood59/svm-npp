\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Machine Learning}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Basic Concepts}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}What are Features?}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Feature Selection}{4}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Feature Extraction}{5}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Finding a Pattern or Relation}{5}{subsubsection.2.1.4}
\contentsline {subsection}{\numberline {2.2}Supervised Learning}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Classification Problems}{6}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Regression Problems}{6}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}Unsupervised Learning}{6}{subsection.2.3}
\contentsline {section}{\numberline {3}Major Machine Learning Algorithms}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Linear Regression}{9}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Logistic Regression}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Support Vector Machine}{12}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Computing the Optimal Hyperplane}{12}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Kernel Trick (Non-Linear Classification)}{14}{subsubsection.3.3.2}
\contentsline {section}{\numberline {4}Applications of SVM in Nuclear Power Plants}{16}{section.4}
\contentsline {subsection}{\numberline {4.1}Estimation of Power Peaking Factor}{16}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Detection and Diagnostics of LOCA}{17}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Predictive based Degradation Monitoring of NPP components}{19}{subsection.4.3}
\contentsline {section}{\nameref {sec:resources}}{21}{section*.14}
\contentsline {section}{\nameref {sec:references}}{21}{section*.15}
